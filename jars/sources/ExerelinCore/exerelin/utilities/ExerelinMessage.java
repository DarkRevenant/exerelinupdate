package exerelin.utilities;

import java.awt.*;

public class ExerelinMessage
{
    public String message;
    public Color color;

    public ExerelinMessage(String message, Color color)
    {
        this.message = message;
        this.color = color;
    }
}
